<!--
    GitLab offers solutions for handling incidents in your 
    applications and services, from setting up an alert with 
    Prometheus, to receiving a notification via a monitoring 
    tool like Slack, and automatically setting up Zoom calls 
    with your support team.

    https://docs.gitlab.com/ee/user/incident_management/#configuring-incidents-ultimate
-->

